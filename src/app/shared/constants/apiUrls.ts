export const apiUrls = {
    city: {
        getAll: '/city/getall',
        getById: 'city/getbyid',
        add: '/city/addcity',
        update: '/city/updatecity',
        delete: '/city/deletecity',
    },
    county: {
        getAll: '/county/getall',
        getById: '/county/getbyid',
        add: '/county/addcounty',
        update: '/county/udpatecounty',
        delete: '/county/deletecounty',
    }
};